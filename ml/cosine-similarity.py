"""
Created on Tue Feb 26 12:04:08 2019
@author: csblo
"""

from scipy.sparse import lil_matrix, csc_matrix, csr_matrix, hstack, random as sparse_random
import numpy as np
import scipy.spatial.distance as distance
a = np.array([0.1, 0.2])
b = np.array([0.3,0.4])

c = 1 - distance.cosine(a, b)
print(c)

m1 = csr_matrix([[1,0.5,0.3], [0.5,0.3,0.2], [0.1,0.2,0.5]])
m2 = csr_matrix([[1,0.5,0.3], [0.5,0.3,0.2], [0.1,0.2,0.5]])
c2 = 1 - distance.cosine(m1, m2)
print(c2)